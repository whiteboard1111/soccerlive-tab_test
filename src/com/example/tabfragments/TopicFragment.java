package com.example.tabfragments;

import java.util.ArrayList;

import android.app.ListFragment;
import android.os.Bundle;

public class TopicFragment extends ListFragment {

	private RssListAdapter mAdapter;
	private ArrayList<Item> mItems;
	private final String url = "http://mexicancats.890m.com/test/";



	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

		mItems = new ArrayList<Item>();
		mAdapter = new RssListAdapter(getActivity(), mItems);

		RssParserTask task = new RssParserTask(getActivity(), mAdapter, this);
		task.execute(url);


	}


}
