package com.example.tabfragments;

import android.graphics.drawable.Drawable;

public class Item {
	// 記事のタイトル
	private CharSequence mTitle;
	// 記事のタイムスタンプ
	private CharSequence mDate;
	// 記事の提供元
	private CharSequence mProvided;
	// 記事元のリンク
	private CharSequence mLink;
	// 画像のURL
	private CharSequence mImgURL;
	// 記事の画像
	private Drawable mImage;
	// 記事の全文
	private CharSequence mDescription;

	// アイテム
	public Item() {
		mTitle = "";
		mDate = "";
		mLink = "";
		mDescription = "";
		mImgURL = "";
		mImage = null;
	}

	// 記事のタイトル
	public CharSequence getTitle() {
		return mTitle;
	}

	public void setTitle(CharSequence title) {
		mTitle = title;
	}

	// 記事の提供元
	public CharSequence getProvided() {
		return mProvided;
	}

	public void setProvided(CharSequence mProvided) {
		this.mProvided = mProvided;
	}

	// 記事のリンク
	public CharSequence getLink() {
		return mLink;
	}

	public void setLink(CharSequence link) {
		mLink = link;
	}

	// 記事の日時
	public CharSequence getDateTime() {
		return mDate;
	}

	public void setDateTime(CharSequence date) {
		mDate = date;
	}

	// 記事の本文
	public CharSequence getDescription() {
		return mDescription;
	}

	public void setDescription(CharSequence Description) {
		mDescription = Description;
	}

	// 記事の画像のURL
	public CharSequence getImgURL() {
		return mImgURL;
	}

	public void setImgURL(CharSequence mImgURL) {
		this.mImgURL = mImgURL;
	}

	// 記事の画像
	public Drawable getImage() {
		return mImage;
	}

	public void setImage(Drawable image) {
		mImage = image;
	}

}