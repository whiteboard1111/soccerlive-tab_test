package com.example.tabfragments;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.app.ListFragment;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Xml;

public class RssParserTask extends AsyncTask<String, RssListAdapter, RssListAdapter> {
	private ListFragment mFragment;
	private ArrayList<Item> mItems;
	private RssListAdapter mAdapter;
	private Context mContext;

	/*
	 * コンストラクタ 引数のactivityはプログレスバーの表示に必要なもの adapterはresultで結果を返すために必要
	 */
	public RssParserTask(Context context, RssListAdapter adapter, ListFragment lf) {
		mAdapter = adapter;
		mContext = context;
		mFragment = lf;
	}

	// サイトのデータを取得し、parseXmlに渡してパース結果を取得する
	@Override
	protected  RssListAdapter doInBackground(String... params) {

		// サイトのデータをparseXmlに渡す
		try {
			mAdapter = parseXml(params[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mAdapter;
	}

	@Override
	protected void onPostExecute(RssListAdapter result) {

		mFragment.setListAdapter(result);
	}

	// XMLをパースするためのメソッド
	public RssListAdapter parseXml(String rss_feed_url) throws IOException,
			XmlPullParserException {

		// XmlPullParserのインスタンスを作成し、サイトデータから取得したURLを格納する
		XmlPullParser parser = Xml.newPullParser();
		URL url = new URL(rss_feed_url);
		InputStream is = url.openConnection().getInputStream();

		try {
			parser.setInput(is, null);
			int eventType = parser.getEventType();
			Item currentItem = null;
			while (eventType != XmlPullParser.END_DOCUMENT) {
				String tag = null;
				switch (eventType) {
				case XmlPullParser.START_TAG:
					tag = parser.getName();
					if (tag.equals("item")) {
						// アイテム要素内
						currentItem = new Item();
					} else if (currentItem != null) {
						if (tag.equals("title")) {
							currentItem.setTitle(parser.nextText());
						} else if (tag.equals("description")) {
							currentItem.setDescription(parser.nextText());
						} else if (tag.equals("pubDate")) {
							currentItem.setDateTime(parser.nextText());
						} else if (tag.equals("link")) {
							currentItem.setLink(parser.nextText());
						} else if (tag.equals("enclosure")) {
							currentItem.setImgURL(parser.nextText());
							// 提供元を取得
						} else if (tag.equals("author")) {
							currentItem.setProvided(parser.nextText());
							// 画像のURLを取得
						} else if (tag.equals("image")) {
							currentItem.setImgURL(parser.nextText());
						}
					}
					break;
				case XmlPullParser.END_TAG:
					tag = parser.getName();
					if (tag.equals("item")) {

						mAdapter.add(currentItem);
					}
					break;

				}
				eventType = parser.next();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mAdapter;
	}
}