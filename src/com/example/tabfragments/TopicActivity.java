package com.example.tabfragments;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class TopicActivity extends Activity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.topic_layout);

		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// トッピクタブを作成
		actionBar.addTab(actionBar
				.newTab()
				.setText("トピック")
				.setTabListener(
						new TabListener<TopicFragment>(this, "tag1",
								TopicFragment.class)));

		// 海外サッカーのタブを作成
		actionBar.addTab(actionBar
				.newTab()
				.setText("海外サッカー")
				.setTabListener(
						new TabListener<WorldSoccerFragment>(this, "tag2",
								WorldSoccerFragment.class)));

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.topic, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
