package com.example.tabfragments;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

public class TabListener<T extends Fragment> implements ActionBar.TabListener {

	private Fragment mFragment;
	private Activity mActivity;
	private String mTag;
	private Class<T> mClass;

	public TabListener(Activity activity, String tag, Class<T> clz) {
		mActivity = activity;
		mTag = tag;
		mClass = clz;
		mFragment = mActivity.getFragmentManager().findFragmentByTag(mTag);
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// タブが選択された際の処理
		if (mFragment == null) {
			mFragment = Fragment.instantiate(mActivity, mClass.getName());

			FragmentManager fm = mActivity.getFragmentManager();
			fm.beginTransaction().add(R.id.container, mFragment, mTag).commit();
		} else {
			if (mFragment.isDetached()) {
				FragmentManager fm = mActivity.getFragmentManager();
				fm.beginTransaction().attach(mFragment).commit();
			}
		}

	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// タブの選択が解除されたとき
		if(mFragment != null){
			FragmentManager fm = mActivity.getFragmentManager();
			fm.beginTransaction().detach(mFragment).commit();
		}

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// 選択されたタブが選択されたとき

	}

}
