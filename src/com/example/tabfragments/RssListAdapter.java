package com.example.tabfragments;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class RssListAdapter extends ArrayAdapter<Item> {
	private LayoutInflater mInflater;
	private TextView mTitle;
	private TextView mDate;
	private ImageView mImage;
	private TextView mSiteName;

	// コンストラクタ
	public RssListAdapter(Context context, List<Item> objects) {
		super(context, R.layout.fragment_list_tab, objects);
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	// staticなビューホルダーを生成し,ビューの取得を簡略化する
	static class ViewHolder {
		TextView titleView;
		TextView dateView;
		TextView siteNameView;
		TextView descrView;
		ImageView imageView;
	}

	// 1行ごとのビューを生成する
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;

		// convertviewがnullであればレイアウト読み込み、あれば使いまわす
		View view = convertView;
		if (convertView == null) {
			view = mInflater.inflate(R.layout.item_row, null);

			TextView titleView = (TextView) view.findViewById(R.id.item_title);
			TextView dateView = (TextView) view.findViewById(R.id.item_date);
			TextView siteNameView = (TextView) view
					.findViewById(R.id.item_site_name);
			ImageView imageView = (ImageView) view
					.findViewById(R.id.item_image);

			//
			holder = new ViewHolder();
			holder.titleView = titleView;
			holder.dateView = dateView;
			holder.siteNameView = siteNameView;
			holder.imageView = imageView;

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		// 現在参照しているリストの位置からItemを取得する
		Item item = this.getItem(position);
		if (item != null) {

			// Itemからタイトルデータを取り出し、TextViewにセットする
			String title = item.getTitle().toString();
			String date = item.getDateTime().toString();
			String siteName = item.getProvided().toString();
			String imageURL = item.getImgURL().toString();

			// 取得した内容を張り付ける
			holder.titleView.setText(title);
			holder.dateView.setText(date);
			holder.siteNameView.setText(siteName);

		}
		return view;
	}

}